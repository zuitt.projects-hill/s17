//alert("Danger Will Robinson!")

//Common examples of an Array

let grades = [98.5, 94.3, 89.2, 90.1];

let marvelHeroes = ["Iron Man", "Thor", "Hulk", "Captain America", "Black Widow", "Hawkeye", "Shang Chi", "Spiderman"];

//Possible use of an array, but not recommended
let mixedArray = [12, "Asus", null, undefined, {}];

//Alternative way to write an array
let myTasks = [
	"drink HTML",
	"eat Javascript",
	"inhale CSS",
	"bake react"

];

//Arrays and Indexes
	//[] - array literals

	// Reassign Values in an array

	console.log(myTasks);
	myTasks[0] = "snort HTML";
	console.log(myTasks);

	// Accessing from an Array
	console.log(grades[2]);
		// 89.2
	console.log(marvelHeroes[6]);
		// Shang Chi
	console.log(myTasks[20]);
		// undefined

	// Getting the length of an array
		// arrays have access to  ".length" property to get the number of elements present in an array.

	console.log(marvelHeroes.length);
		// 8

	if(marvelHeroes.length > 5){
		console.log("We have too many heroes, please contact Thanos")

	};

	//Accessing the last element of an array
		//Since the first element of an array starts at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element.

	let lastElement = marvelHeroes.length-1
	console.log(marvelHeroes[lastElement]);
		// Spiderman

	// Array Methods
		// Mutator Methods
			// these are functions that "mutate" or change an array after they are created.

		let fruits = ["Apple", "Blueberry", "Orange", "Grapes"]

		//push ()
			// adds an element in the end of an array and returns the array length.

		console.log(fruits);
		let fruitsLength = fruits.push("Mango");
		console.log(fruits);

		// Adding multiple elements
		fruits.push("Guava", "Kiwi");
		console.log(fruits);

		// pop ()
			// removes the last element in an array and returns the removed element

		let removedFruit = fruits.pop();
		console.log(removedFruit);
		console.log(fruits);

		fruits.pop();
		console.log(fruits);

		//Unshift ()
			//adds an element at the beginning of an array

		fruits.unshift("Guava", "Kiwi", "Lime");
		console.log(fruits);

		// shift()
			// removing an element at the beginning of an array

		fruits.shift();
		console.log(fruits);

		/*
			note: 
			push and unshift - adding elements
			pop and shift - removing elements
		*/

		//Splice()
			//simultaneously removes elements from a specified index number and edds elements
			/*
				Syntax: arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

			*/

		fruits.splice(1, 2, "Cherry", "Watermelon");
		console.log(fruits);

		//removes from index to the last element
		fruits.splice(3);
		console.log(fruits);

		fruits.splice(1, 1);
		console.log(fruits);

		//adding in the middle of an array
		fruits.splice(1, 0, "Buko", "Dorian");
		console.log(fruits);

	//Sort()
		//rearranges the array elements in alphanumeric order

		fruits.sort();
		console.log(fruits);

	//reverse()
		//reverves the order of the array elements

		fruits.reverse();
		console.log(fruits);



//Non-Mutator Methods
	//these functions do not modify or change an array.

	let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"]

	// indexOf()
		//returns the index number of the first matching element found in an array
		// Syntax: arrayName.indexOf(searchable element)
	console.log(countries.indexOf("PH"));
		// 1 , because it only shows the first match of the search value

	let indexOfSG = countries.indexOf("SG");
	console.log(indexOfSG);
		// 3

	let invalidCountry = countries.indexOf("SK");
	console.log(invalidCountry);
		// -1

	let invalidCountry2 = countries.indexOf("JP");
	console.log(invalidCountry2);
		// -1

	//slice()
		//slices elements from an array and returns a new array
		//Syntax: 
		//	arrayName.slice(startingIndex);
		//	arrayName.slice(startingIndex, endingIndex);

		//slice of element from a specified index to the last element

		let slicedArrayA = countries.slice(2);
		console.log(slicedArrayA);
			//["CAN", "SG", "TH", "PH", "FR", "DE"]

		// slice off elements from specified index to another index

		let slicedArrayB = countries.slice(2, 4);
		console.log(slicedArrayB);
			//["CAN","SG"]

		//slice off elements starting from the last element of an array

		let slicedArrayC = countries.slice(-3);
		console.log(slicedArrayC);
			// ['PH', 'FR', 'DE']


	//toString()
		// returns an array as string separated by commas
		// Syntax: arrayName.toString();

	let stringArray = countries.toString();
	console.log(stringArray);
		// US,PH,CAN,SG,TH,PH,FR,DE

	let sentence = ["I", "like", "javascript", ".", "It's", "fun", "!"]

	let sentenceString = sentence.toString();
	console.log(sentenceString);
		//I,like,javascript,.,It's,fun,!


	//concat()
		//combines two arrays and returns a combined result
		//Syntax: arrayA.concat(arrayB);
			//arrayA.concat(arrayB);
			//array

	let tasksArrayA = ["drink HTML", "eat javascript"];
	let tasksArrayB = ["inhale CSS", "breathe sass"];
	let tasksArrayC = ["get git", "be node"];

	let tasks = tasksArrayA.concat(tasksArrayB);
	console.log(tasks);

	// Combing multiple arrays

	let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
	console.log(allTasks);

	//Combing array with elements

	let combinedTasks = tasksArrayA.concat("smell express", "throw react");
	console.log(combinedTasks);
		//['drink HTML', 'eat javascript', 'smell express', 'throw react']

	//join()
		//returns an array as a string separated by a specified separator string

	let members = ["Rose", "Lisa", "Jisoo", "Jennie"];

	let joinedMembers1 = members.join();
	console.log(joinedMembers1);
	// comma as default separator

	let joinedMembers2 = members.join('');
	console.log(joinedMembers2);
	//'' no spaces

	let joinedMembers3 = members.join(" ");
	console.log(joinedMembers3);
	//" " spaces

	let joinedMembers4 = members.join("-");
	console.log(joinedMembers4);
	//"-" dashes

  //Iteration Method 
  	// they are loops designed to perform repetitive tasks on arrays
  	// useful for manipulating array data resulting in complex tasks
  	//have a function inside an array

	  // forEach()

	  	// an empty array will store the filtered elements in the iteration method. This is to avoid confusion by modifying the original array.

		let filteredTasks = [];

	  allTasks.forEach(function(task){
	  	console.log(task)

	  	if(task.length > 10){
	  		filteredTasks.push(task);
	  	};

	  });

	  console.log(filteredTasks);

	  //map()
	  	//iterates on each element and returns new array with different values
	  	// unlike forEach method, map method reuires the use of return keyword in order to create another array with the performed operation.
	  
	  let numbers = [1, 2, 3, 4, 5];
	  let numberMap = numbers.map(function(number){
	  	return number * number;
	  	
	  });

	console.log(numberMap);

	//every()
		// checks is all elements meet a certain condition
		// returns a true value if all elements meet the condition and false if they don't

		let allValid = numbers.every(function(number){
			return (number < 3);
		});
		console.log(allValid);
		// false

	// some()
		// checks at least one element in the array that meets the condition
		// returns a true value if at least one element meets the condition, false if otherwise

		let someValid = numbers.some(function(number){
			return (number < 2);
		});

		console.log(someValid);
		// true

	// filter()
		// returns a new array that contains the elements that meet a certain condition

		let filterValid = numbers.filter(function(number){
			return (number < 3);
		});
		console.log(filterValid);
			// [1,2]


	let colors = ["Red", "Green", "Black", "Orange", "Yellow"];

	let values = ["Red", "Black", "Yellow"]

	colors = colors.filter(item => values.indexOf(item) === -1);

	console.log(colors);

	// includes()
		// returns a boolean value true if it finds a matching item in the array
		//includes is case-sensitive

	let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

	let filteredProducts = products.filter(function(product){
		return product.toLowerCase().includes("a");

	})
	console.log(filteredProducts);


	// Multidimensional Array
		//useful for storing complex data structures

	let chessboard = [
		["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
		["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
		["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
		["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
		["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
		["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
		["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
		["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]

	]

	console.log(chessboard[1][4]);
	console.log(`Queen moves to ${chessboard[5][7]}`);
	