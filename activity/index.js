//alert("It's Linked!")

let students = [];

function addStudent(name){

	students.push(name);
	console.log(`${name} was added to the student's list.`);

}

function countStudents(){

	console.log(`There are a total of ${students.length} students enrolled.`);
}

function printStudents(){

	students.sort();

	students.forEach(function(student){
		console.log(student);
	})
	
};

function findStudent(keyword){
	let match = students.filter(function(student) {
		return student.toLowerCase().includes(keyword.toLowerCase());
	})
};

if (match.length == 1){
	console.log(`${match} is an Enrollee'`);
} else if (match.length > 1){
	console.log(`${match} are Enrollees`);
} else {
	console.log(`No student found with the name ${keyword}.`)
}